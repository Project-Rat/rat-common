// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef CMN_ERROR_HH
#define CMN_ERROR_HH

// general headers
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <cstring>

#include "typedefs.hh"

// filename macro for shortening the path
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

// custom runtime_error implementation
// in order to include filename and line number
class rat_error: public std::runtime_error{
	private:
		std::string msg;
	public:
		// constructor
		rat_error(const std::string &arg, const char *file, const char *function, int line) :
		std::runtime_error(arg) {
			std::ostringstream o;
			o << KRED << KBLD << "error: " << KNRM << file << ":" << function << ":" << 
				KGRN << KBLD << line << KNRM << ": " << std::endl << " " KYEL << arg << KNRM;
			msg = o.str();
		}
		
		// destructor
		~rat_error() throw() {}
		const char *what() const throw() override{
			return msg.c_str();
		}
};

// macro for error
#define rat_throw_line(arg) throw rat_error(arg, __FILENAME__, __FUNCTION__, __LINE__);

#endif