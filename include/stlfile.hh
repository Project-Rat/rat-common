// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef CMN_STL_FILE_HH
#define CMN_STL_FILE_HH

// general headers
#include <boost/filesystem.hpp>
#include <armadillo>
#include <memory>
#include <list>

#include "typedefs.hh"

// code specific to Rat
namespace rat{namespace cmn{
	// shared pointer definition for log
	typedef std::shared_ptr<class STLFile> ShSTLFilePr;



	// logging to the terminal
	class STLFile{
		public:
			// data storage
			struct MeshData{
				arma::Mat<arma::uword> t;
				arma::Mat<fltp> R;
			};

		// properties
		private:
			// store triangle data
			boost::filesystem::path fname_;

			// meshes
			std::list<MeshData> meshes_;

		// methods 
		public:
			// constructor
			explicit STLFile(const boost::filesystem::path &fname);

			// destructor
			~STLFile();

			// factory
			static ShSTLFilePr create(const boost::filesystem::path &fname);

			// add triangles no need to re-index
			void add_triangles(const MeshData &data);

			// write nodes
			void write();
	};

}}

#endif