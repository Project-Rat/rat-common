// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// this code was translated to C++ an adapted from:
// https://ch.mathworks.com/matlabcentral/fileexchange/43097-newton-raphson-solver

// include guard
#ifndef CMN_RUNGE_KUTTA_HH
#define CMN_RUNGE_KUTTA_HH

// general headers
#include <armadillo> 
#include <memory>
#include <iomanip>
#include <functional>
#include <cassert>
#include <list>

// specific headers
#include "typedefs.hh"
#include "log.hh"

// code specific to Rat
namespace rat{namespace cmn{

	// shared pointer definition
	typedef std::shared_ptr<class RungeKutta> ShRungeKuttaPr;

	// system function
	typedef std::function<arma::Mat<fltp>(
		const fltp tt, const arma::Mat<fltp>&yy) > RKSysFun;
	typedef std::function<int(const arma::uword iter, 
		const fltp tt, const fltp dt, const arma::sword event_detected) > RKMonitorFun;
	typedef std::function<arma::Row<fltp>(
		const fltp tt, const arma::Mat<fltp>&yy) > RKEventFun;

	// timestep data
	struct RKTimeData{
		fltp tt;
		arma::Row<fltp> yy;
		arma::Row<fltp> yp;
	};

	// output
	struct RKData{
		arma::Col<fltp> tt;
		arma::Mat<fltp> yy;
		arma::Mat<fltp> yp;
		arma::sword event_detected;
	};

	// newton raphson non-linear solver
	class RungeKutta{
		// enums
		public:
			// integration method used by the tracking algorithm
			enum class IntegrationMethod{
				EULER_FORWARD,
				RALSTON,
				RUNGE_KUTTA3,
				RUNGE_KUTTA4,
				RUNGE_KUTTA_FEHLBERG45,
				DORMAND_PRINCE,
				KRAAIJEVANGER_SPIJKER,
				CRANK_NICOLSON,
				CASH_EDIRK23,
				AL_RABEH_EDIRK34,
				HAIRER_WANNER_EDIRK34
			};

		// properties
		private:
			// butcher's tableau
			std::string methodname_;
			std::string schemename_;
			arma::uword order_ = 0llu;
			arma::Mat<fltp> a_;
			arma::Mat<fltp> b_;
			arma::Col<fltp> c_;

			// system function
			RKSysFun system_fun_;

			// monitoring function
			RKMonitorFun monitor_fun_;

			// event function
			RKEventFun event_fun_;

			// tolerance
			fltp reltol_ = RAT_CONST(1e-6);
			fltp abstol_ = RAT_CONST(1e-6);

			// root finding tolerance
			fltp roottol_ = RAT_CONST(1e-4); // relative to time step

			// initial step
			fltp dtini_ = RAT_CONST(1e-4);

			// maximum number of iterations
			arma::uword max_iter_ = 10000llu;

			// root finding bisection max iter
			arma::uword root_finding_max_iter_ = 100llu;

			// diagonal implicit settings
			fltp dirk_damping_ = 1.0;
			arma::uword dirk_max_iter_ = 100llu;

			// minimum step size
			fltp min_allowable_step_size_ = RAT_CONST(1e-12);

		// methods
		public:
			// constructor
			RungeKutta();
			explicit RungeKutta(const IntegrationMethod integration_method);

			// factory
			static ShRungeKuttaPr create();
			static ShRungeKuttaPr create(const IntegrationMethod integration_method);

			// set tolerances
			void set_abstol(const fltp abstol);
			void set_reltol(const fltp reltol);
			void set_roottol(const fltp roottol);
			void set_max_iter(const arma::uword max_iter);

			// get tolerances
			fltp get_abstol()const;
			fltp get_reltol()const;
			fltp get_roottol()const;
			arma::uword get_max_iter()const;

			// check if it is a fehlberg method
			bool is_embedded()const;

			// get order
			arma::uword get_order()const;

			// set init ial time step
			void set_dtini(const fltp dtini);

			// set the system function
			void set_system_fun(const RKSysFun& system_fun);

			// set the monitoring function
			void set_monitor_fun(const RKMonitorFun& monitor_fun);

			// set the event function
			void set_event_fun(const RKEventFun& event_fun);

			// set butcher table directly
			void set_butcher_tableau(
				const arma::Mat<fltp> &a,
				const arma::Row<fltp> &b,
				const arma::Col<fltp> &c,
				const std::string& methodname = "custom");

			void set_butcher_tableau(
				const IntegrationMethod integration_method);

			// display butcher's tableau
			void display_butchers_tableau(const ShLogPr& lg);

			// perform single integration step
			arma::field<arma::Mat<fltp> > perform_step(
				const fltp t, const fltp dt,
				const arma::Mat<fltp> &yy,
				const fltp step_tolerance = RAT_CONST(0.0));

			// integrate
			RKData integrate(
				const fltp t1, const fltp t2,
				const arma::Row<fltp> &y0,
				const arma::Row<fltp> &yp0 = {}); // if not set zeros will be used
	};

}}

#endif
