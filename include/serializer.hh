// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef CMN_SERIALIZER_HH
#define CMN_SERIALIZER_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <functional>
#include <json/json.h>
#include <cstdlib>
#include <set>
#include <boost/filesystem.hpp>

#include "node.hh"
#include "log.hh"

// code specific to Rat
namespace rat{namespace cmn{

	// shared pointer definition
	typedef std::shared_ptr<class Serializer> ShSerializerPr;

	// serialization code for distmesh models
	class Serializer{
		// registration
		protected:
			// list of object constructors
			NodeFactoryMap factory_list_;

			// json root
			Json::Value root_json_;

			// path to root
			boost::filesystem::path pth_;

		// methods
		public:
			// constructor
			Serializer();
			explicit Serializer(const ShNodePr &root_node);
			explicit Serializer(const boost::filesystem::path &fname);

			// destructor
			virtual ~Serializer(){};

			// factory
			static ShSerializerPr create();
			static ShSerializerPr create(ShNodePr root_node);
			static ShSerializerPr create(const boost::filesystem::path &fname);

			// check root function
			bool has_valid_json_root() const;

			// import/export to text file
			void import_json(const boost::filesystem::path &fname);
			void export_json(const boost::filesystem::path &fname);

			// access to json tree
			Json::Value& root_json();
			void clear_json();

			// tree construction
			ShNodePr construct_tree_core();
			void flatten_tree(const Node* root_node);
			void flatten_tree(const ShNodePr& root_node);
			template<typename T> std::shared_ptr<T> construct_tree();

			// registration
			void register_factory(const std::string &str,NodeFactory factory);
			void override_factory(const std::string &str,NodeFactory factory);
			template<typename T> void register_factory();
			template<typename T> void override_factory();
			void list_factories(ShLogPr lg) const;

			// get factory list
			NodeFactoryMap get_factory_list() const;

			// method for registering constructors
			// to be overridden
			virtual void register_constructors();

			// creation
			template<typename T> std::shared_ptr<T> json2tree(const std::string &fname);

			// export to string
			std::string export_string() const;
			bool import_string(const std::string &str);

			// copy function
			template<typename T> std::shared_ptr<T> copy(const std::shared_ptr<T> &input_node);
			template<typename T> std::shared_ptr<T> copy(const T* input_node);
	};

	// template functions (must be in header file)
	template<typename T> std::shared_ptr<T> Serializer::construct_tree(){
		std::shared_ptr<T> tree = std::dynamic_pointer_cast<T>(construct_tree_core());
		if(tree==NULL)rat_throw_line("could not cast tree to requested type: " + T::get_type());
		return tree;
	}

	// template functions (must be in header file)
	template<typename T> std::shared_ptr<T> Serializer::json2tree(const std::string &fname){
		if(factory_list_.size()==0)register_constructors(); 
		import_json(fname); return construct_tree<T>();
	}

	// alternative registration using template
	template<typename T> void Serializer::register_factory(){
		register_factory(T::get_type(), []()->rat::cmn::ShNodePr{return std::make_shared<T>();} );
	}

	// alternative registration using template
	template<typename T> void Serializer::override_factory(){
		override_factory(T::get_type(), []()->rat::cmn::ShNodePr{return std::make_shared<T>();} );
	}

	// copy function with shared pointer input
	template<typename T> 
	std::shared_ptr<T> Serializer::copy(const std::shared_ptr<T> &input_node){
		if(input_node==NULL)return NULL;
		flatten_tree(input_node); 
		return construct_tree<T>();
	}

	// copy function with raw pointer input
	template<typename T> 
	std::shared_ptr<T> Serializer::copy(const T* input_node){
		if(input_node==NULL)return NULL;
		flatten_tree(input_node); return construct_tree<T>();
	}

}}

#endif
