// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef CMN_GAUSS_HH
#define CMN_GAUSS_HH

// general headers
#include <armadillo> 
#include <complex>
#include <cmath>
#include <cassert>

#include "typedefs.hh"

// code specific to Rat
namespace rat{namespace cmn{
	// that are used throughout the code
	class Gauss{
		private:
			// settings
			fltp tol_ = RAT_CONST(1e-7);
			arma::sword num_gauss_ = 3;

			// abscissas
			arma::Row<fltp> xg_;
			
			// weights
			arma::Row<fltp> wg_;

		// methods
		public:
			// constructor
			Gauss();
			explicit Gauss(const arma::sword num_gauss, const fltp tol = RAT_CONST(1e-7));

			// setters
			void set_num_gauss(const arma::sword num_gauss);
			void set_tol(const fltp tol);

			// getters
			fltp get_tol()const;
			arma::sword get_num_gauss()const;

			// calculation
			void calculate();
			const arma::Row<fltp>& get_abscissae() const;
			const arma::Row<fltp>& get_weights() const;	

	};

}}
 
#endif