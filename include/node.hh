// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef CMN_NODE_HH
#define CMN_NODE_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <list>
#include <map>
#include <set>
#include <json/json.h>
#include <boost/filesystem.hpp>

#include "typedefs.hh"
#include "error.hh"

// disable warning about inheritance via dominance (which is used a lot for node) when compiling in windows
#ifdef _WIN32
#pragma warning(disable : 4250) 
#endif

// code specific to Rat
namespace rat{namespace cmn{

	// shared pointer definition
	typedef std::shared_ptr<class Node> ShNodePr;
	typedef arma::field<ShNodePr> ShNodePrList;

	// node con
	typedef ShNodePr(*NodeFactory)();
	typedef std::map<std::string,NodeFactory> NodeFactoryMap;

	// list types
	typedef std::map<ShNodePr,arma::uword> SList;
	typedef std::map<arma::uword,ShNodePr> DSList;

	// circle distance function for distmesh
	class Node{
		// properties
		protected:
			// name of this node
			std::string myname_ = "";

			// menu status (for gui)
			bool enable_ = true;
			bool tree_open_ = false;

		// methods
		public:
			// default constructor
			Node(){};
			virtual ~Node(){};

			// serialization of the nodes
			static Json::Value serialize_node(const ShNodePr &node, SList &list);
			static ShNodePr deserialize_node_core(
				const Json::Value &js, DSList &list, 
				const NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth);
			template<typename T> static std::shared_ptr<T> deserialize_node(
				const Json::Value &js, DSList &list, 
				const NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth);

			// json io functions
			static void write_json(const boost::filesystem::path &fname, const Json::Value &js);
			static Json::Value parse_json(const boost::filesystem::path &fname, const bool is_compressed = false);

			// json io functions with compression
			static void write_json_compressed(const boost::filesystem::path &fname, const Json::Value &js);
			static Json::Value parse_json_compressed(const boost::filesystem::path &fname);
			static void fix_file(const boost::filesystem::path &fname);

			// setters
			void set_name(const std::string &name);
			void append_name(const std::string &ext, const bool append_back = false);
			void set_tree_open(const bool tree_open = true);
			void set_enable(const bool enable = true);

			// getters
			const std::string& get_name()const;
			bool get_tree_open()const;
			bool get_enable()const;
			void toggle_enable();
			void toggle_tree_open();

			// input validity check when 
			// hard_check is true an error will be thrown
			virtual bool is_valid(const bool enable_throws = false) const;

			// tree structure
			virtual ShNodePr get_tree_node(const arma::uword index) const;
			virtual arma::uword add_tree_node(const ShNodePr &node);
			virtual bool delete_tree_node(const arma::uword index);
			virtual arma::uword get_num_tree_nodes() const;
			virtual void reindex(){};

			// get/add multiple nodes
			std::list<ShNodePr> get_tree_nodes() const;
			arma::Row<arma::uword> add_tree_nodes(const std::list<ShNodePr> &nodes);
			void clear_tree_nodes();

			// check if node can be added
			bool is_addable(const cmn::ShNodePr &node);

			// static functions for serializing armadillo matrices 
			static Json::Value serialize_matrix(const arma::Mat<fltp> &V);
			static arma::Mat<fltp> deserialize_matrix(const Json::Value &js);
            static Json::Value serialize_cube(const arma::Cube<fltp>& V);
            static arma::Cube<fltp> deserialize_cube(const Json::Value& js);
			static Json::Value serialize_uword_matrix(const arma::Mat<arma::uword> &V);
			static arma::Mat<arma::uword> deserialize_uword_matrix(const Json::Value &js);
			static std::string fix_filesep(const std::string& path);

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, SList &list) const;
			virtual void deserialize(
				const Json::Value &js, 
				std::map<arma::uword,ShNodePr> &list, 
				const NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth);
	};

	// template function (must be in header)
	template<typename T> 
	std::shared_ptr<T> Node::deserialize_node(
		const Json::Value &js, DSList &list, 
		const NodeFactoryMap &factory_list,
		const boost::filesystem::path &pth){
		
		// get node
		const ShNodePr node = Node::deserialize_node_core(js, list, factory_list, pth);
		
		// allocate output shared pointer
		std::shared_ptr<T> castnode;

		// check if the node was NULL to begin with
		// in this case the node was not set
		if(node!=NULL){
			// try to cast the node to required type
			castnode = std::dynamic_pointer_cast<T>(node);

			// check if the node was cast to null
			// this means that the type does not match
			if(castnode==NULL)
				std::cout<<"warning: downcast failed: "<<js["type"].asString()<<" to "<<T::get_type()<<std::endl;
				// rat_throw_line("downcast failed: " + js["type"].asString() + " to " + T::get_type());

			else if(!castnode->is_valid(false))
				std::cout<<"warning: node failed validity check: "<<node->get_name()<<" "<<typeid(node).name()<<std::endl;
		}

		// return downcast node
		return castnode;
	}

}}

#endif
