// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// this code was translated to C++ an adapted from:
// https://ch.mathworks.com/matlabcentral/fileexchange/43097-newton-raphson-solver

// include guard
#ifndef CMN_NEWTON_RAPHSON_HH
#define CMN_NEWTON_RAPHSON_HH

// general headers
#include <armadillo> 
#include <memory>
#include <iomanip>
#include <functional>
#include <cassert>

// specific headers
#include "typedefs.hh"
#include "log.hh"

// code specific to Rat
namespace rat{namespace cmn{
	// shared pointer definition
	typedef std::shared_ptr<class NewtonRaphson> ShNewtonRaphsonPr;

	// jacobian function
	typedef std::function<arma::Mat<fltp>(const arma::Col<fltp>&) > NRJacFun;

	// system function
	typedef std::function<arma::Col<fltp>(const arma::Col<fltp>&) > NRSysFun;

	// newton raphson non-linear solver
	class NewtonRaphson{
		// properties
		private:
			// settings
			bool use_parallel_ = false;
			bool use_central_diff_ = false;
			fltp tolx_ = RAT_CONST(1e-12); // x tolerance
			fltp tolfun_ = RAT_CONST(1e-6); // function tolerance
			arma::uword num_iter_max_ = 150; // maximum number of iterations
			fltp alpha_ = RAT_CONST(1e-4); // criteria for decrease
			fltp min_lambda_ = RAT_CONST(0.1); // minimum lambda
			fltp max_lambda_ = RAT_CONST(0.5); // maximum lambda
			fltp delta_ = RAT_CONST(1e-6); // finite difference stepsize

			// solver functions
			NRSysFun systemfun_;
			NRJacFun jacfun_;
			
			// initial guess vector
			arma::Col<fltp> x0_; 

			// solution vector
			arma::Col<fltp> x_; 

			// internal jacobian matrix
			arma::Mat<fltp> J_;

			// output statistics
			arma::sword flag_ = 0;
			arma::uword num_iter_;
			fltp resnorm_; // residual norm
			fltp stepnorm_;
			fltp lambda_; // lambda
			fltp rc_; // reciprocal condition
			arma::Col<fltp> F_; // function value
			arma::Col<fltp> dx_; // stepsize

		// methods
		public:
			// constructor
			NewtonRaphson();

			// factory
			static ShNewtonRaphsonPr create();

			// setting of system function and start point
			void set_systemfun(NRSysFun fn);
			void set_jacfun(NRJacFun fn);

			// jacobian function with finite difference
			void set_finite_difference();
		
			// set initial guess for x
			void set_initial(const arma::Col<fltp> &x0);

			// settings
			void set_use_parallel(const bool use_parallel);
			void set_use_central_diff(const bool central_diff);
			
			void set_display(const bool display);
			void set_tolx(const fltp tolx);
			void set_tolfun(const fltp tolfun);
			void set_delta(const fltp delta);
			void set_num_iter_max(const arma::uword num_iter_max);
			
			// solver functions
			void solve(ShLogPr lg = NullLog::create());

			// getting
			arma::Col<fltp> get_result() const;

			// finite difference approximation of the jacobian matrix
			// this will be used if no viable jacobian function assigned
			static arma::Mat<fltp> approximate_jacobian(const arma::Col<fltp> &x, const fltp dx, const bool use_parallel, const bool central_diff, NRSysFun sysfn);
	};

}}

#endif
