// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef CMN_MARCHING_CUBES_HH
#define CMN_MARCHING_CUBES_HH

#include <memory>
#include <armadillo>
#include <list>

// rat common headers
#include "typedefs.hh"

// using tables and concept from Paul Bourke
// http://www.paulbourke.net/geometry/polygonise/

// code specific to Rat GUI
namespace rat{namespace cmn{

	// shared pointer definition
	typedef std::shared_ptr<class MarchingCubes> ShMarchingCubesPr;

	// class for editing float
	class MarchingCubes{
		// tables
		private:
			// check
			bool is_setup_ = false;

			// tolerance (with respect to values)
			fltp tolerance_ = RAT_CONST(1e-9);

			// edge table
			arma::Row<int> edge_table_;

			// triangulation table
			arma::Mat<int> tri_table_;

			// edge list
			arma::Mat<arma::uword> edge_list_; 
			
			// counters
			arma::uword num_nodes_input_;
			arma::uword num_nodes_output_;
			arma::uword max_num_output_elements_;

		// methods
		public:
			// constructor
			MarchingCubes();
			
			// factory
			static ShMarchingCubesPr create();

			// set tolerance
			void set_tolerance(const fltp tolerance);

			// Linearly interpolate the position where an isosurface cuts
			// an edge between two vertices, each with their own scalar value
			arma::Col<fltp> vertex_interp(
				const fltp level,
				const arma::Col<fltp> &p1,
				const arma::Col<fltp> &p2,
				const fltp valp1,
				const fltp valp2,
				const fltp tol) const;

			// polygonise a cube
			arma::Mat<fltp> polygonise(
				const arma::Mat<fltp> &R, 
				const arma::Row<fltp> &val, 
				const fltp level  = 0.0) const;

			// perform marching cubes on a 3D grid
			arma::Mat<fltp> polygonise(
				const fltp xmin, const fltp xmax, const arma::uword num_x, 
				const fltp ymin, const fltp ymax, const arma::uword num_y, 
				const fltp zmin, const fltp zmax, const arma::uword num_z, 
				const arma::Row<fltp> &values, const fltp level = 0.0);

			// perform marching cubes on a 2D grid
			arma::Mat<fltp> polygonise(
				const fltp xmin, const fltp xmax, const arma::uword num_x, 
				const fltp ymin, const fltp ymax, const arma::uword num_y, 
				const arma::Row<fltp> &values, const fltp level = 0.0);

			// perform marching cubes on a mesh
			arma::Mat<fltp> polygonise(
				const arma::Mat<fltp> &Rn, 
				const arma::Row<fltp> &vals,
				const arma::Mat<arma::uword> &n,
				const fltp level = 0.0);

			// hexahedron to iso-triangles
			void setup_hex2isotri(); // classical marching cubes
			void setup_tet2isotri();
			void setup_quad2tri();
			void setup_quad2isoline(); // classical marching squares
			void setup_tri2tri();
			void setup_tri2isoline();
			void setup_line2line();
			void setup_line2isopoint();
			void setup_point2point();


			// helper function for combining line elements
			static arma::field<arma::Mat<fltp> > combine_line_segments(const arma::Mat<fltp>& R);
	};



}}

#endif