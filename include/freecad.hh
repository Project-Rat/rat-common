// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef CMN_FREECAD_HH
#define CMN_FREECAD_HH

// general headers
#include <armadillo> 
#include <memory>
#include <iostream>
#include <iomanip>
#include <cassert>
#include <boost/filesystem.hpp>
#include <stdio.h>

// specific headers
#include "extra.hh"

// code specific to Rat
namespace rat{namespace cmn{
	// shared pointer definition
	typedef std::shared_ptr<class FreeCAD> ShFreeCADPr;

	// template for coil
	class FreeCAD{
		// properties
		protected:
			// file out stream
			std::ofstream fid_; 

			// map of used names
			std::map<std::string, arma::uword> coil_names_;

			// set scaling
			fltp scale_ = RAT_CONST(1000.0);

			// subdivision settings
			bool enable_refine_ = true; // keep subdividing to get angle under 90 deg
			arma::uword num_refine_max_ = 100llu; // maximum number of subsections when refining
			arma::uword num_sub_ = 1llu; // enforce subdivision into this number of subsections
			arma::uword num_max_ = 100000llu; // maximum number of nodes in subsections

			// switches
			bool override_ruled_surface_ = true; // use ruled surface method even if generator lines are not perpendicular
			bool enable_gui_update_ = false; // allow the GUI to update during building (slower)
			bool no_graphics_ = false; // do not use any graphics (for command line builds)
			bool create_union_ = false; // try to create a union by combining all parts of each coil
			bool keep_construction_ = true; // keep construction geometry

			// check name uniqueness
			std::map<std::string, arma::uword> coil_name_count_;
		
		// methods
		public:
			// constructor
			FreeCAD(
				const boost::filesystem::path &fname, 
				const std::string &model_name);

			// destructor
			~FreeCAD();

			// factory
			static ShFreeCADPr create(
				const boost::filesystem::path &fname, 
				const std::string &model_name);

			// set number of subdivisions
			void set_num_sub(const arma::uword num_sub);
			void set_num_max(const arma::uword num_max);
			void set_num_refine_max(const arma::uword num_refine_max);
			void set_no_graphics(const bool no_graphics = true);
			void set_keep_construction(const bool keep_construction = true);
			void set_create_union(const bool create_union = true);
			void set_enable_gui_update(const bool enable_gui_update = true);
			void set_override_ruled_surface(const bool override_ruled_surface = true);
			void set_enable_refine(const bool enable_refine = true);
			void set_scale(const fltp scale);

			// grouping
			void append_group(const std::string &group_name);
			void pop_group();

			// write freecad python script file
			void write_header(const std::string &model_name);
			void write_edges(
				const arma::field<arma::Mat<fltp> > &x,
				const arma::field<arma::Mat<fltp> > &y, 
				const arma::field<arma::Mat<fltp> > &z, 
				const arma::Row<arma::uword> &section, 
				const arma::Row<arma::uword> &turn, 
				const std::string &coil_name, 
				const arma::Col<float>::fixed<3>& color = {0.54f,0.78f,0.78f});
			void write_section(
				const arma::Mat<fltp> &x, 
				const arma::Mat<fltp> &y, 
				const arma::Mat<fltp> &z, 
				const arma::uword section_idx, 
				const arma::uword turn_idx, 
				const arma::uword sub_idx, 
				const std::string &coil_name, 
				const arma::Col<float>::fixed<3>& color);
			void write_section2(
				const arma::Mat<fltp> &x, 
				const arma::Mat<fltp> &y, 
				const arma::Mat<fltp> &z, 
				const arma::uword section_idx, 
				const arma::uword turn_idx, 
				const arma::uword sub_idx, 
				const std::string &coil_name, 
				const arma::Col<float>::fixed<3>& color);
			void write_section3(
				const arma::Mat<fltp> &x, 
				const arma::Mat<fltp> &y, 
				const arma::Mat<fltp> &z, 
				const arma::uword section_idx, 
				const arma::uword turn_idx,
				const arma::uword sub_idx, 
				const std::string &coil_name, 
				const arma::Col<float>::fixed<3>& color);
			void write_pointlist(arma::uword num_points);
			void write_footer();

			// special operations
			void create_fusion(const std::string& coil_name);
			void group_parts(const std::string& coil_name);

			// write saves
			void save_doc(const boost::filesystem::path &fname);
			void save_step_file(const boost::filesystem::path &fname);

			// function for checking if two edges are a ruled surface
			static bool is_ruled_surface(
				const arma::Mat<fltp>& xx, 
				const arma::Mat<fltp>& yy, 
				const arma::Mat<fltp>& zz, 
				const arma::uword idx1, 
				const arma::uword idx2, 
				const rat::fltp tol);
			static bool is_closed(
				const arma::Mat<fltp>& x, 
				const arma::Mat<fltp>& y, 
				const arma::Mat<fltp>& z, 
				const rat::fltp tol);
			
			// name making function
			static std::string create_section_name(
				const arma::uword turn_idx,
				const arma::uword section_idx, 
				const arma::uword sub_idx);
			
	};

}}

#endif
